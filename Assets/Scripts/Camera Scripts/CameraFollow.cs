﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This script is used to follow the action of the game in 
//the general overworld, cutscenes, and conversations

[DisallowMultipleComponent]
public class CameraFollow : MonoBehaviour {

    [Header("Settings")]
    public bool followingPlayer = true;
    [Range(0,1, order = 32)]
    public float smoothSpeed = 0.125f;

    [Tooltip("The minimum X constraint")]
    public float leftRoomConstraint = -10f;

    [Tooltip("The maximum X constraint")]
    public float rightRoomConstraint = 10f;

    [Tooltip("The minimum Z constraint")]
    public float backRoomConstraint = -10f;

    [Tooltip("The maximum Z constraint")]
    public float frontRoomConstraint = 10f;

    [Tooltip("The minimum Y constraint")]
    public float bottomRoomConstraint = -10f;

    [Tooltip("The maximum Y constraint")]
    public float topRoomConstraint = 10f;

    public float lastY = 0;//Last Y used to remember the last Y coord before jumping.

    [Header("Offsets")]
    public Vector3 generalOffset;
    public Vector3 conversationOffset;

    [Header("Transforms")]
    public Transform followPoint;
    public Transform playerRigTransform;

    [Header("Misc")]
    public PlayerMovement playerMovement;

    // Start is called before the first frame update
    void Start() {
        
    }

    private void FixedUpdate() {
        Follow();
    }

    private void Follow() {
        if (followingPlayer) {

            Vector3 desiredPosition = new Vector3();

            //X
            if (playerRigTransform.position.x > leftRoomConstraint && playerRigTransform.position.x < rightRoomConstraint) {
                desiredPosition.x = followPoint.position.x;
            }
            else if (playerRigTransform.position.x < leftRoomConstraint) { //If reached left constraint, don't move further
                desiredPosition.x = leftRoomConstraint;
            }
            else if (playerRigTransform.position.x > rightRoomConstraint) { //If reached right constraint, don't move further
                desiredPosition.x = rightRoomConstraint;
            }

            //Z
            if (playerRigTransform.position.z > backRoomConstraint && playerRigTransform.position.z < frontRoomConstraint) {
                desiredPosition.z = followPoint.position.z;
            }
            else if (playerRigTransform.position.z < backRoomConstraint) { //If reached back constraint, don't move further
                desiredPosition.z = backRoomConstraint;
            }
            else if (playerRigTransform.position.z > frontRoomConstraint) { //If reached front constraint, don't move further
                desiredPosition.z = frontRoomConstraint;
            }


            //Follow Y, ignoring Jumping
            //Y
            if (playerRigTransform.position.y > bottomRoomConstraint && playerRigTransform.position.y < topRoomConstraint && playerMovement.hasLanded) {// && ! playerMovement.hasJumped
                desiredPosition.y = followPoint.position.y;
            }
            else if (playerRigTransform.position.y < bottomRoomConstraint) { //If reached bottom constraint, don't move further
                desiredPosition.y = bottomRoomConstraint;
            }
            else if (playerRigTransform.position.y > topRoomConstraint) { //If reached top constraint, don't move further
                Debug.Log("Above my top constraint!");
                desiredPosition.y = topRoomConstraint;
            }
            else {
                desiredPosition.y = lastY - generalOffset.y; //Helps ignore jump
            }

            if (playerMovement.grounded && playerMovement.hasLanded) {
                lastY = transform.position.y; //Helps the script remember where the player last was on the Y axis before jumping.
            }

            desiredPosition = desiredPosition + generalOffset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;

        }
    }

    public void InsertNewConstraints(Vector3 minimumConstraints, Vector3 maximumConstraints) {

    }
}
