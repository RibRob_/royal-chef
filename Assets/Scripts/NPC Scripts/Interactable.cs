﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {

    //[Header("Settings")]
    public SpriteRenderer interactableIconRenderer;
    public bool canInteract = false;
    public float fadeSpeed = 4f;
    public Color spriteColor;
    public PlayerInteraction playerInteraction;

    // Start is called before the first frame update
    void Start() {
        spriteColor = interactableIconRenderer.color;
    }

    // Update is called once per frame
    void Update() {
        DisplayIcon();
    }

    public virtual void GetInteractedWith() {
    }

    protected void DisplayIcon() {
        if (canInteract && spriteColor.a < 1) {
            spriteColor.a += Time.deltaTime * fadeSpeed;
            interactableIconRenderer.color = spriteColor;
        }
        else if (!canInteract && spriteColor.a > 0) {
            spriteColor.a -= Time.deltaTime * fadeSpeed;
            interactableIconRenderer.color = spriteColor;
        }
    }
}
