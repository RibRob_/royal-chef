﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Entity", menuName = "Entity")]
public class Entity : ScriptableObject {

    [Header("Info")]
    public string entityName = "";

    [Header("Health")]
    [SerializeField] protected int _currentHealth = 20;
    [SerializeField] protected int _maxHealth = 20;
    [SerializeField] protected int _defense = 0;

    [Header("Sprites and Animations")]
    public Sprite characterSprite;

    public int CurrentHealth {
        get { return _currentHealth; }
    }

    public int MaxHealth {
        get { return _maxHealth; }
    }

    protected virtual void CheckHealth() {
        if (_currentHealth <= 0 || _maxHealth <= 0) {
            //Death sequence
            return;
        }

        if (_currentHealth > _maxHealth) {
            _currentHealth = _maxHealth;
        }
    }

    public void ChangeCurrentHealth(int amount, string damageType) {
        if (damageType != "") {
            _currentHealth += amount + _defense;
            //Damage animation
        }
        else {
            _currentHealth += amount;
        }
        CheckHealth();

        //Add event
    }

    public void ChangeMaxHealth(int amount) {
        _maxHealth += amount;
        if (amount < 0) {
            ChangeCurrentHealth(amount, "drain");
        }
        else if (amount > 0) {
            ChangeCurrentHealth(amount, "");
        }
    }
}
