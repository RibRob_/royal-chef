﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Talkable : Interactable {

    public DialogueBase dialogue;
    public bool interactedWith = false;
    public bool hasTalked = false;
    public Scene playerScene;
    public DialogueManager dialogueManager;


    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        DisplayIcon();
        Talk();
    }

    public override void GetInteractedWith() {
        if (canInteract) {
            interactedWith = true;
        }
    }

    private void Talk() {
        if(interactedWith){
            if (dialogueManager != null) {
                TriggerDialogue();
            }
            interactedWith = false;
        }
    }

    private void TriggerDialogue() {
        if (!hasTalked) {
            hasTalked = true;
            DialogueManager.instance.EnqueueDialogue(dialogue);
            canInteract = false;
        }
    }
}
