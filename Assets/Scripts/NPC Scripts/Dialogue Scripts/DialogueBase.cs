﻿//Rob Harwood
//Used Tutorial: https://youtu.be/Yri0C-E7xG4
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "New Dialogue", menuName = "Dialogues")]
public class DialogueBase : ScriptableObject {

    [System.Serializable]
    public class Info {
        //public Entity entity;
        public string name;
        [TextArea(3, 5)]
        public string dialogue;
        public Transform bodyLocation;
        public Sprite dialogueBoxSprite;
        public GameObject talkingCharacter;
        public AudioClip typeVoice;
    }

    [Header("Insert Dialogue Information Below")]
    public Info[] dialogueInfo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
