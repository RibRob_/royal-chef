﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class SpriteShadowCaster : MonoBehaviour
{

    public SpriteRenderer m_spriteRenderer;

    // Start is called before the first frame update
    void Start() {
        m_spriteRenderer.receiveShadows = true;
        m_spriteRenderer.shadowCastingMode = ShadowCastingMode.On;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
