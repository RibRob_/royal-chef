﻿//Rob Harwood
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Initializer : MonoBehaviour {

    public string initialScene = "";
    public GameObject errorMessageGO;
    public TextMeshProUGUI errorMessageText;

    private void Start() {
        //Load the initial scene the player is supposed to be in (PlayerScene is always loaded first)
        SceneManager.LoadScene(initialScene, LoadSceneMode.Additive);
    }

}
