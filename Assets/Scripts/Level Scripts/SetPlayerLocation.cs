﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//This actually works, omg.
//Accesses the player in its player scene and puts it in the right location
//for the level.

public class SetPlayerLocation : MonoBehaviour {

    public Scene playerScene;
    public GameObject playerRig;
    public Transform startingPosition;

    // Start is called before the first frame update
    void Start() {
        playerScene = SceneManager.GetSceneByName("PlayerScene");

        GameObject[] rootObjects = playerScene.GetRootGameObjects();

        if (rootObjects.Length > 0) {
            foreach (GameObject go in rootObjects) {
                if (go.name == "Player") {
                    playerRig = go;
                }
            }
        }

        if (playerRig != null) {

            //Check what exit the player last collided with to put them in correct position.
            playerRig.transform.position = startingPosition.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
