﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class SetUpLevel : MonoBehaviour {

    //public Scene playerScene;
    public DialogueManager dialogueManager;
    public PlayerInteraction playerInteraction;
    public PlayerMovement playerMovement;
    //public GameObject canvas;
    //GameObject[] rootPlayerSceneGOs;
    public List<Talkable> talkingCharacters = new List<Talkable>();


    private void Awake() {
        playerInteraction = PlayerInteraction.instance;
        playerMovement = PlayerMovement.instance;
        dialogueManager = DialogueManager.instance;
        SetUpTalkingCharacters();
    }

    // Start is called before the first frame update
    void Start() {
        //OLD CODE NO LONGER NECESSARY. DELETE IF NOT RUNNING INTO ISSUES
        //playerScene = SceneManager.GetSceneByName("PlayerScene");
        //rootPlayerSceneGOs = playerScene.GetRootGameObjects();

        //foreach (GameObject go in rootPlayerSceneGOs) {
        //    FindPlayerScripts(go);
        //    SetUpTalkingCharacters(go);
        //}

        //playerMovement.canWalk = true;
    }

    
    private void FindPlayerScripts(GameObject go){
        //OLD CODE NO LONGER NECESSARY. DELETE IF NOT RUNNING INTO ISSUES
        //if (playerActions != null) {
        //    return;
        //}

        //if (go.name == "Player") {
        //    playerActions = go.GetComponent<PlayerActions>();
        //    playerMovement = go.GetComponent<PlayerMovement>();
        //    playerMovement.canWalk = false;
        //}

    }

    private void SetUpTalkingCharacters() {
        //OLD CODE NO LONGER NECESSARY. DELETE IF NOT RUNNING INTO ISSUES
        ////If done already, don't need to do anything else
        //if (dialogueManager != null) {
        //    return;
        //}

        //if (go.name == "Canvas") {
        //    canvas = go;

        //    DialogueManager[] dialogueManagersFound = canvas.GetComponentsInChildren<DialogueManager>();
        //    if (dialogueManagersFound.Length > 0) {
        //        dialogueManager = dialogueManagersFound[0];
        //    }
        //}


        foreach (Talkable t in talkingCharacters) {
            t.dialogueManager = dialogueManager;
            t.playerInteraction = playerInteraction;
        }
    }
}
