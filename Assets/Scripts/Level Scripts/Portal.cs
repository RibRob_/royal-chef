﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour {

    public string leadToScene = "";

    public void Teleport() {
        //Fade to black
        //Pause time
        //Make sure player is safe
        //Load new level
        //Unpause time
        //Unload this level
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Player") {
            collision.gameObject.SendMessage("UpdatePortal", this.name);
            Teleport();
        }
    }
}
