﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : ScriptableObject {
    public string name = "";
    [TextArea(3,5)] public string description = "";
    public Sprite itemSprite;

}