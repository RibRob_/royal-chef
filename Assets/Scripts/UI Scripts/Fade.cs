﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fade : MonoBehaviour {

    [Header("Settings")]
    public float fadeSpeed = 2f;
    public CanvasGroup canvasGroup;
    private bool _fadedIn = false;

    public bool FadedIn {
        get { return _fadedIn; }
    }

    // Update is called once per frame
    void Update(){
        FadeIn();
    }

    private void FadeIn() {
        if (_fadedIn) {
            canvasGroup.alpha += Time.deltaTime * fadeSpeed;
        }
        else {
            canvasGroup.alpha -= Time.deltaTime * fadeSpeed;
        }
    }

    public void StartFade() {
        _fadedIn = !_fadedIn;
    }
}