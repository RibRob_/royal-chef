﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerHUD : MonoBehaviour {


    public TextMeshProUGUI healthText;
    public TextMeshProUGUI staminaText;
    public TextMeshProUGUI coinsText;
    public Player player;


    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        UpdateText();
    }

    private void UpdateText() {
        healthText.text = player.CurrentHealth.ToString();
        staminaText.text = player.CurrentStamina.ToString();
        coinsText.text = player.Coins.ToString();

    }
}
