﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class ButtonBackground : MonoBehaviour, ISelectHandler, IDeselectHandler, IPointerEnterHandler, IPointerExitHandler {

    public GameObject buttonBackground;

    public void OnPointerEnter(PointerEventData pointerEventData) {
        buttonBackground.SetActive(true);
    }

    public void OnPointerExit(PointerEventData pointerEventData) {
        buttonBackground.SetActive(false);
    }

    public void OnSelect(BaseEventData baseEventData) {
        buttonBackground.SetActive(true);
    }

    public void OnDeselect(BaseEventData baseEventData) {
        buttonBackground.SetActive(false);
    }

}
