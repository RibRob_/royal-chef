﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DevTools : MonoBehaviour {

    [Header("Access Combo")]
    public string combo1 = "Left Shift";
    public string combo2 = "Tab";
    public string combo3 = "t";
    public char deliminator = ' ';

    [Header("Misc")]
    public GameObject devTool;
    public TMP_InputField devInput;
    public PlayerMovement playerMovement;
    public Player playerStats;

    //Under the hood stuff
    private string _enteredCheatCode;


    // Start is called before the first frame update
    void Start() {
        devTool.SetActive(false);
    }

    // Update is called once per frame
    void Update() {
        CheckForCombo();
        CheckForInput();
    }

    private void CheckForCombo() {
        if (Input.GetButton(combo1) && Input.GetButton(combo2) && Input.GetKeyDown(combo3)) {

            if (!devTool.activeInHierarchy) {
                devTool.SetActive(true);
                playerMovement.canWalk = false;
                playerMovement.canJump = false;
            }
            else {
                devTool.SetActive(false);
                playerMovement.canWalk = true;
                playerMovement.canJump = true;
            }
        }
    }

    private void CheckForInput() {
        if (devTool.activeInHierarchy && Input.GetKeyDown(KeyCode.Return)) {
            _enteredCheatCode = devInput.text;
            devInput.text = "";
            Debug.Log("Entered Cheat Code: " + _enteredCheatCode);
            BreakDownCode(_enteredCheatCode);
        }
    }

    private void BreakDownCode(string cheatCode) {
        //If nothing significatn was entered, ignore it.
        if (cheatCode == "" || cheatCode == " ") {
            return;
        }

        //Break down the code
        string[] words = cheatCode.Split(deliminator);

        if ((words[0] == "add" || words[0] == "Add") && words.Length == 3) {
            int amount;
            int.TryParse(words[2], out amount);
            Add(words[1], amount);
        }


    }

    private void Teleport(string sceneName) {

    }

    private void Add(string stat, int amount) {
        Debug.Log("Added " + amount + " " + stat);
        if (stat == "health") {
            playerStats.ChangeCurrentHealth(amount, "");
        }
        if (stat == "maxhealth") {
            playerStats.ChangeMaxHealth(amount);
        }
    }
}
