﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour {

    [Header("Primary Settings")]
    public float typeDelay = 0.01f;
    public Color normalTextColor;
    public Color importantTextColor;
    public List<Animator> advancedDialogueTextContainer = new List<Animator>();
    public List<TextMeshProUGUI> advancedDialogueTextList = new List<TextMeshProUGUI>();

    [Header("Player Scripts")]
    public PlayerInteraction playerInteraction;
    public PlayerMovement playerMovement;

    [Space]
    public CanvasGroup dialogueCanvasGroup;
    public Image dialogueSpeechBubble;
    public Sprite defaultSpeechBubble;
    public TextMeshProUGUI dialogueText;
    public GameObject talkingCharacter;
    public GameObject dialogueGO;
    public GameObject playerHUDGO;
    public AudioSource soundPlayer;
    public AudioClip defaultTextSound;
    public bool talking = false;
    public GameObject dialogueHolder;
    public PauseMenu pauseMenu;

    public Queue<DialogueBase.Info> dialogueInfo = new Queue<DialogueBase.Info>();


    public static DialogueManager instance;

    //Private varialbes
    private bool isCurrentlyTyping = false;
    private string completeMessage = "";
    private Coroutine _typer;

    private void Awake() {
        if (instance != null) {
            Debug.LogWarning("More than 1 instance, fix this");
        }
        else {
            instance = this;
        }


    }

    private void Update() {
        GetNextLine();
    }

    /// <summary>
    /// Prepares the dialogue for being read
    /// </summary>
    /// <param name="dialogueBase"></param>
    public void EnqueueDialogue(DialogueBase dialogueBase) {
        //Make Queue empty
        dialogueInfo.Clear();

        //Populate Queue
        foreach (DialogueBase.Info info in dialogueBase.dialogueInfo) {
            dialogueInfo.Enqueue(info);
        }

        CapturePlayer();
        DequeueDialogue();
    }

    /// <summary>
    /// Puts dialogue into the dialogue box
    /// </summary>
    public void DequeueDialogue() {
        if (isCurrentlyTyping) {
            CompleteText();
            StopCoroutine(_typer);
            return;
        }


        //Check if Queue is empty, if so end sequence
        if (dialogueInfo.Count <=0 ) {
            ReleasePlayer();
            return;
        }


        DialogueBase.Info info = dialogueInfo.Dequeue();
        completeMessage = info.dialogue;

        //Make sure something is there, otherwise, use a default sprite
        if (info.dialogueBoxSprite != null) {
            dialogueSpeechBubble.sprite = info.dialogueBoxSprite;
        }
        else {
            dialogueSpeechBubble.sprite = defaultSpeechBubble;
        }

        //Make sure something is there, otherwise, let the dev know
        if (info.dialogue != null) {
            dialogueText.text = info.dialogue;
        }
        else {
            dialogueText.text = "NO MESSAGE...";
        }

        //AnimateDialogue.instance.StopAnimating();
        dialogueText.text = "";
        //Empty advanced dialogue
        EmptyAdvancedDialogue();

        dialogueGO.SetActive(true);
        _typer = StartCoroutine(TypeText(info));

        //Replace with some kind of animation
        //talkingCharacter = info.talkingCharacter;
    }


    IEnumerator TypeText(DialogueBase.Info info) {
        isCurrentlyTyping = true;

        SetTypeAudio(info);

        char formatCharacter1 = ' ';
        char formatCharacter2 = ' ';
        int skipped = 0;
        for (int i = 0; i < info.dialogue.Length; i++) {
            if (i - skipped >= advancedDialogueTextList.Count) {
                Debug.Log("Line was too long.");
                break;
            }
            yield return new WaitForSeconds(typeDelay);

            char c = info.dialogue[i];
            if (c == '<') {
                if (formatCharacter1 == ' ') {
                    formatCharacter1 = info.dialogue[i + 1];
                }

                i += 3;
                skipped += 3;
                c = info.dialogue[i];

                if (c == '<' && formatCharacter2 == ' ') {
                    Debug.Log("second format character picked");
                    formatCharacter2 = info.dialogue[i + 1];
                    i += 3;
                    skipped += 3;
                }
            }
            else if(c == ' ' || c == '!' || c == '.' || c == ',' || c == '?'){
                formatCharacter1 = ' ';
                formatCharacter2 = ' ';
            }

            StyleDialogue(i, skipped, formatCharacter1, formatCharacter2);

            soundPlayer.Play();
            advancedDialogueTextList[i - skipped].text += info.dialogue[i];
        }

        isCurrentlyTyping = false;
        pauseMenu.canPause = true;
    }

    /// <summary>
    /// Makes all the dialogue go back to a nuetral state
    /// </summary>
    private void EmptyAdvancedDialogue() {
        for (int i = 0; i < advancedDialogueTextList.Count; i++) {
            advancedDialogueTextList[i].text = "";
            advancedDialogueTextList[i].fontStyle = FontStyles.Normal;
            advancedDialogueTextList[i].color = normalTextColor;

            advancedDialogueTextContainer[i].Play("Idle", 0);
        }
    }

    private void StyleDialogue(int index, int skipped, char formatCharacter1, char formatCharacter2) {
        if (formatCharacter1 != ' ') {

            if (formatCharacter1 == 'b' || formatCharacter2 == 'b') {
                advancedDialogueTextList[index - skipped].fontStyle = FontStyles.Bold;
            }
            if (formatCharacter1 == 'i' || formatCharacter2 == 'i') {
                advancedDialogueTextList[index - skipped].color = importantTextColor;
            }
            if (formatCharacter1 == 's' || formatCharacter2 == 's') {
                advancedDialogueTextContainer[index - skipped].Play("SlideSideways", 0);
            }
            if (formatCharacter1 == 'w' || formatCharacter2 == 'w') {
                advancedDialogueTextContainer[index - skipped].Play("SlideUp", 0);
            }
        }
    }

    //private void CheckIfStyled(out char format1, out char format2) {
    //}

    //FIGURE OUT HOW TO MAKE CODE REUSABLE
    private void CompleteText() {
        foreach (TextMeshProUGUI tmpGUI in advancedDialogueTextList) {
            tmpGUI.text = "";
        }

        char formatCharacter1 = ' ';
        char formatCharacter2 = ' ';
        int skipped = 0;
        for (int i = 0; i < completeMessage.Length; i++) {
            if (i - skipped >= advancedDialogueTextList.Count) {
                Debug.Log("Line was too long.");
                break;
            }


            char c = completeMessage[i];
            if (c == '<') {
                if (formatCharacter1 == ' ') {
                    formatCharacter1 = completeMessage[i + 1];
                }

                i += 3;
                skipped += 3;
                c = completeMessage[i];

                if (c == '<' && formatCharacter2 == ' ')
                {
                    Debug.Log("second format character picked");
                    formatCharacter2 = completeMessage[i + 1];
                    i += 3;
                    skipped += 3;
                }
            }
            else if (c == ' ' || c == '!' || c == '.' || c == ',' || c == '?') {
                formatCharacter1 = ' ';
                formatCharacter2 = ' ';
            }

            StyleDialogue(i, skipped, formatCharacter1, formatCharacter2);


            advancedDialogueTextList[i - skipped].text += completeMessage[i];

        }

        isCurrentlyTyping = false;
    }

    private void SetTypeAudio(DialogueBase.Info info) {
        if (info.typeVoice == null) {
            soundPlayer.clip = defaultTextSound;
        }
        else {
            soundPlayer.clip = info.typeVoice;
        }
    }


    /// <summary>
    /// Allows the player to cycle through the dialogue using the interaction button
    /// </summary>
    public void GetNextLine() {
        if (talking) {
            if (Input.GetButtonDown(playerInteraction.interact) || Input.GetKeyDown(KeyCode.Return)) {
                instance.DequeueDialogue();
            }
        }
    }

    /// <summary>
    /// Prevents the player from moving
    /// </summary>
    public void CapturePlayer() {
        pauseMenu.canPause = false;
        playerInteraction.DisableInput();
        playerMovement.DisableInput();
        talking = true;
        playerHUDGO.SetActive(false);
        dialogueGO.SetActive(true);
    }

    /// <summary>
    /// Allows the player to move
    /// </summary>
    public void ReleasePlayer() {
        pauseMenu.canPause = true;
        playerInteraction.EnableInput();
        playerMovement.EnableInput();
        talking = false;
        dialogueGO.SetActive(false);
        playerHUDGO.SetActive(true);
    }
}
