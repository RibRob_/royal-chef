﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    [Header("Input Strings")]
    public string pause = "Escape";

    [Header("Player Scripts")]
    public PlayerMovement playerMovement;
    public PlayerInteraction playerInteraction;
    public EventSystem eventSystem;

    [Header("Bools")]
    public bool canPause = true;
    public bool isPaused = false;
    public bool isResuming = false;

    [Header("GameObjects")]
    public GameObject pauseMenuGO;
    public GameObject playerHUDGO;
    public GameObject pauseMenuButtonContainer;
    public GameObject partyMenuButtonContainer;
    public GameObject[] pauseMenuButtons;

    [Header("Animation Settings")]
    public Animator pauseMenuAnimations;
    public float closeScreenSpeed = 0.25f;
    public float buttonAppearDelay = 0.1f;

    [Header("First Selected Buttons")]
    public Button resumeButton;

    [HideInInspector] public static PauseMenu instance;
    //Private settings
    private string menuLocation = "Pause Menu";
    private string lastPlayedAnimation = "";
    private Coroutine _makeAppear;
    private Coroutine _makeDisappear;


    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        if (canPause) {
            OnPlayerPause();

        }
    }

    private void OnPlayerPause() {
        if (Input.GetButtonDown(pause) && !isPaused) {
            Debug.Log("Pause");
            menuLocation = "Pause Menu";
            pauseMenuAnimations.speed = closeScreenSpeed;
            pauseMenuAnimations.SetFloat("Direction", 1f);
            pauseMenuAnimations.Play("Close-Screen-PauseMenu", 0);
            isPaused = true;
            isResuming = false;
            Time.timeScale = 0;
            playerMovement.DisableInput();
            playerInteraction.DisableInput();
            playerHUDGO.SetActive(false);
        }
        else if ((Input.GetButtonDown(pause) || isResuming) && isPaused) {
            Debug.Log("Unpause button hit");
            OnResume();
        }
    }

    public void ScreenOpen() {
        if (!isPaused) {
            Debug.Log("Unpause");
            menuLocation = "None";
            isResuming = false;
            Time.timeScale = 1;
            playerMovement.EnableInput();
            playerInteraction.EnableInput();
            playerHUDGO.SetActive(true);
            pauseMenuAnimations.speed = 0;
        }
    }

    public void ScreenClose() {
        if (menuLocation != "Party") {
            pauseMenuAnimations.speed = 0;
            //Make character appear
            //Show HP, Stamina, and Coins
            //Make buttons appear
            pauseMenuButtonContainer.SetActive(true);
            //_makeAppear = StartCoroutine(MakeAppear1by1(pauseMenuButtons));
            //MakeAppear1by1(pauseMenuButtons);
            resumeButton.Select();
        }
    }

    //BUTTON FUNCTIONALITY

    public void OnResume() {
        ScreenOpen();
        isResuming = true;
        pauseMenuButtonContainer.SetActive(false);
        isPaused = false;
        pauseMenuAnimations.speed = closeScreenSpeed;
        pauseMenuAnimations.SetFloat("Direction", -1f);
        pauseMenuAnimations.Play("Close-Screen-PauseMenu", 0);
        //Animation calls ScreenOpen
    }

    public void BackToPauseMenu() {
        menuLocation = "Pause Menu";
        DisableAllButtonContainers();
        pauseMenuAnimations.speed = 0.5f;
        pauseMenuAnimations.SetFloat("Direction", -1f);
        pauseMenuAnimations.Play(lastPlayedAnimation, 0);
    }

    public void ShowParty() {
        Debug.Log("Party button hit");
        menuLocation = "Party";
        pauseMenuButtonContainer.SetActive(false);
        pauseMenuAnimations.speed = 0.5f;
        pauseMenuAnimations.SetFloat("Direction", 1f);
        pauseMenuAnimations.Play("Make-Sidebar-PauseMenu", 0);
        lastPlayedAnimation = "Make-Sidebar-PauseMenu";
    }

    public void PartyAfterAnimation() {
        pauseMenuAnimations.speed = 0;
        partyMenuButtonContainer.SetActive(true);
        //Make info slide in
    }

    public void AnimationSequence() {

    }

    public void ShowInventory() {
    }

    public void ShowSkills() {
    }

    public void ShowSettings() {
    }

    public void SaveGame() {
    }

    public void Quit() {
    }

    private void DisableAllButtonContainers() {
        pauseMenuButtonContainer.SetActive(false);
        partyMenuButtonContainer.SetActive(false);
    }
}
