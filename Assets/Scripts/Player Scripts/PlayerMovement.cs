﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

[DisallowMultipleComponent]
public class PlayerMovement : MonoBehaviour {

    [Header("Input Strings")]
    public string moveZInput = "Vertical";
    public string moveXInput = "Horizontal";
    public string sprint = "Left Shift";
    public string jump = "Space";

    //Controller settings
    private bool _playerIndexSet = false;
    private PlayerIndex _playerIndex;
    private GamePadState _state;
    private GamePadState _prevState;

    [Header("Sprites and Animations")]
    public SpriteRenderer playerBody;
    public Sprite playerFrontRightSprite;

    [Header("Jump Settings")]
    public float jumpForce = 10f;
    public float fallMultiplier = 3f;
    public float groundedSkin = 0.1f;
    public float bufferDistance = 0.25f;
    public bool grounded = true;
    public bool hasLanded = true;
    public Rigidbody playerRigidBody;
    public LayerMask groundMask;
    public BoxCollider playerCollider;
    public Vector3 boxCenter;
    Vector3 boxSize = new Vector3(0.5f, 0.5f, 0.25f);
    private Collider[] _colliders;
    public float randomJumpOffRange = 1f;

    //Movement settings
    private float _fbMovement;
    private float _lrMovement;


    [Header("Sprint Settings")]
    public float currentSpeed = 3f;
    public float walkSpeed = 3f;
    public float runSpeed = 5f;
    public ParticleSystem sprintParticles;

    //[Header ("Movement Info")]
    private Vector3 _totalMovement;

    [Header("Enabled Settings")]
    public bool canWalk = true;
    public bool canSprint = true;
    public bool canJump = true;
    [Space]
    public string lastPortalCollidedWith = "";

    [HideInInspector]
    public static PlayerMovement instance;

    private void Awake() {
        if (instance != null) {
            Debug.LogWarning("More than 1 instance, fix this");
        }
        else {
            instance = this;
        }
    }


    // Start is called before the first frame update
    void Start() {
        boxSize = new Vector3(playerCollider.size.x - bufferDistance, groundedSkin, playerCollider.size.z - bufferDistance);
        sprintParticles.Stop();
        Debug.Log("Connected: " + _state.IsConnected);
    }

    void FixedUpdate() {
        Sprint();
        Walk();
    }

    private void Update() {
        CheckController();
        Jump();
    }

    private void Sprint() {
        if (canSprint) {
            //If sprint button or left stick is held.
            if (Input.GetButton(sprint) || (_state.Buttons.LeftStick == ButtonState.Pressed)) {
                currentSpeed = runSpeed;
            }
            else if (currentSpeed > walkSpeed) {
                currentSpeed = walkSpeed;
                sprintParticles.Stop();
            }

            //If sprint button is down, and sprint particles aren't already
            //playing, then play particles
            if ((Input.GetButton(sprint) || (_state.Buttons.LeftStick == ButtonState.Pressed)) && !sprintParticles.isPlaying) {
                sprintParticles.Play();
                Debug.Log("Play particles sprint pressed");
            }

            //If sprint button is released, stop playing sprint particles
            if (Input.GetButtonUp(sprint) || (_state.Buttons.LeftStick == ButtonState.Released && _prevState.Buttons.LeftStick == ButtonState.Pressed)) {
                sprintParticles.Stop();
                Debug.Log("Stop particles, sprint released");
            }

            //If not on ground, then stop playing sprint particles
            if (!grounded) {
                sprintParticles.Stop();
                Debug.Log("Stop particles, not grounded");
            }

            //If not moving, then stop playing sprint particles
            if (_lrMovement == 0 && _fbMovement == 0) {
                sprintParticles.Stop();
                Debug.Log("Stop particles, no movement");
            }
        }
    }

    private void Walk() {
        if (canWalk) {
            //If the gamepad is not connected, use keyboard input.
            if (!_state.IsConnected) {
                _fbMovement = Input.GetAxisRaw(moveZInput);
                _lrMovement = Input.GetAxisRaw(moveXInput);
            }
            else {
                _fbMovement = _state.ThumbSticks.Left.Y;//Input.GetAxisRaw(moveZInput);
                _lrMovement = _state.ThumbSticks.Left.X;//Input.GetAxisRaw(moveXInput);
            }

            //Determine sprite and animation here later
            if (_lrMovement > 0 && playerBody.flipX) {
                //playerBody.flipX = false;
            }
            else if (_lrMovement < 0 && !playerBody.flipX) {
                //playerBody.flipX = true;
            }

            _totalMovement = new Vector3(_lrMovement, 0, _fbMovement);
            _totalMovement.Normalize();
            this.transform.Translate(_totalMovement * currentSpeed * Time.deltaTime);
        }
    }

    private void Jump() {
        if (canJump) {
            //If pressed the jump button or pressed A on the gamepad.
            if ((Input.GetButtonDown(jump) || (_prevState.Buttons.A == ButtonState.Released && _state.Buttons.A == ButtonState.Pressed)) && grounded) {
                playerRigidBody.velocity = Vector3.up * jumpForce;
                hasLanded = false;
                sprintParticles.Stop();
            }
            else {
                //Calculate the center of the box to be cast
                boxCenter = (Vector3)transform.position + Vector3.down * (playerCollider.size.y + groundedSkin) * 0.5f;

                //Get all colliders in box
                _colliders = Physics.OverlapBox(boxCenter, boxSize / 2, this.transform.rotation, groundMask);

                //If there are no colliders at all, then the foreach loop will not run.
                //so if the array is empty, then the player is not touching ANY colliders
                //and is therefore not grounded.
                if (_colliders.Length == 0) {
                    grounded = false;
                }

                //Check if any are walkable. If so, you're grounded.
                foreach (Collider c in _colliders) {
                    if (c.tag == "Walkable") {

                        //If grounded was false earlier, and you're holding the
                        //Sprint button, then turn on the sprint particles.
                        if (grounded == false && Input.GetAxisRaw(sprint) > 0) {
                                sprintParticles.Play();
                        }
                        grounded = true;
                        break;
                    }
                    else {
                        grounded = false;
                    }
                }
            }
        }

        //Gravity multiplier if player is at peak of arch to create a snappy and satisfying
        //end to the arch
        if (playerRigidBody.velocity.y <= 0) {
            playerRigidBody.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
    }

    private void CheckController() {
        // Will find the first controller that is connected ans use it
        if (!_playerIndexSet || !_prevState.IsConnected)
        {
            for (int i = 0; i < 4; ++i)
            {
                PlayerIndex testPlayerIndex = (PlayerIndex)i;
                GamePadState testState = GamePad.GetState(testPlayerIndex);
                if (testState.IsConnected)
                {
                    Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
                    _playerIndex = testPlayerIndex;
                    _playerIndexSet = true;
                }
            }
        }

        _prevState = _state;
        _state = GamePad.GetState(_playerIndex);
    }

    public void DisableInput() {
        canWalk = false;
        canSprint = false;
        canJump = false;
    }

    public void EnableInput() {
        canWalk = true;
        canSprint = true;
        canJump = true;
    }

    public void UpdatePortal(string portalName) {
        lastPortalCollidedWith = portalName;
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.tag == "Walkable" && grounded) {
            hasLanded = true;
            canWalk = true;
            playerRigidBody.velocity = new Vector3(0, 0, 0); //Prevents sliding
        }

        //Bounce off NPC
        if (collision.gameObject.tag == "NPC" && !grounded) {
            foreach (Collider c in _colliders) {
                if (c.tag == "NPC") {

                    _totalMovement.Normalize();
                    Vector3 newDirection;

                    //If not moving, give a direction, else reverse original direction.
                    if (Input.GetAxisRaw(moveXInput) == 0 && Input.GetAxisRaw(moveZInput) == 0) {
                        newDirection = new Vector3(Random.Range(-randomJumpOffRange, randomJumpOffRange), 0, Random.Range(-randomJumpOffRange, randomJumpOffRange));
                        newDirection = newDirection * (jumpForce / 2);
                    }
                    else {
                        newDirection = _totalMovement * (jumpForce / 2) * -1;
                    }

                    newDirection.y = jumpForce / 2;
                    playerRigidBody.velocity = newDirection;//Apply bounce
                    canWalk = false;//Player cannot make the player walk
                }
            }

        }

    }

    private void OnDrawGizmos() {
        //Draws the box used to tell if the player is grounded
        Gizmos.DrawWireCube(boxCenter, boxSize);
    }
}
