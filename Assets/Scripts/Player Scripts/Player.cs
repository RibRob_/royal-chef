﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Player", menuName = "Player")]
public class Player : Entity {

    [Header("Stats")]
    [SerializeField] private int _currentStamina = 20;
    [SerializeField] private int _maxStamina = 20;
    [SerializeField] private int _coins = 0;
    [SerializeField] private int _skillPoints = 2;
    [SerializeField] private int _skillSlotsAvailable = 0;
    [SerializeField] private int _currentEXP = 0;
    [SerializeField] private int _neededEXP = 100;
    [SerializeField] private int _level = 1;

    [Header("Collections")]
    public List<PartyMember> party = new List<PartyMember>();
    public List<Items> importantItems = new List<Items>();
    public List<Items> ingredients = new List<Items>();
    public List<Items> consumables = new List<Items>();
    public List<Items> recipes = new List<Items>();
    //Weapons?

    [HideInInspector]
    public Player instance;

    private void Awake() {
        if (instance != null) {
            Debug.LogWarning("More than 1 instance, fix this");
        }
        else {
            instance = this;
        }
    }

    public int CurrentStamina {
        get { return _currentStamina; }
    }

    public int Coins {
        get { return _coins; }
    }
}
