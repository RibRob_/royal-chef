﻿//Rob Harwood
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class LevelLoader : MonoBehaviour {

    public string lastPortalCollidedWith = "";
    public Portal portal;
    public Scene playerScene;
    public UnityEvent loadingNewLevel;

    // Start is called before the first frame update
    void Start() {
    }

    // Update is called once per frame
    void Update() {
        
    }


    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Portal") {
            //Set portal name
            lastPortalCollidedWith = other.name;

            //Fade to black event
            loadingNewLevel.Invoke();

            //Load new scene
            portal = other.gameObject.GetComponent<Portal>();
            portal.Teleport();
        }
    }
}
