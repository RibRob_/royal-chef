﻿//Rob Harwood
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using XInputDotNetPure;

public class PlayerInteraction : MonoBehaviour {

    [Header("Input Strings")]
    public string interact = "Enter";

    [Header("Interact Stuff")]
    public bool canInteract = true;
    public Interactable lastTriggeredInteractable;
    public List<Interactable> interactables = new List<Interactable>();

    [HideInInspector]
    public static PlayerInteraction instance;
    private void Awake() {
        if (instance != null) {
            Debug.LogWarning("More than 1 instance, fix this");
        }
        else {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        Interact();
    }

    private void Interact() {
        if (canInteract) {
            //If you press button and you have triggered an interactable
            if ((Input.GetButtonDown(interact) || Input.GetKeyDown(KeyCode.Return)) && lastTriggeredInteractable != null) {
                lastTriggeredInteractable.GetInteractedWith();
            }
        }
    }

    public void DisableInput() {
        canInteract = false;
    }

    public void EnableInput() {
        canInteract = true;
    }

    protected void OnTriggerEnter(Collider other) {
        if (other.tag == "NPC") {
            //If an interactable was already triggered, then set it to false
            //since a new one was triggered
            if (lastTriggeredInteractable != null) {
                lastTriggeredInteractable.canInteract = false;
            }

            //The new one is the one the player can interact with
            lastTriggeredInteractable = other.GetComponent<Interactable>();
            lastTriggeredInteractable.canInteract = true;
        }
    }

    protected void OnTriggerExit(Collider other) {
        if (other.tag == "NPC") {
            Interactable interactableLeft = other.GetComponent<Interactable>();

            if (interactableLeft != null) {
                interactableLeft.canInteract = false;

                Talkable character = TryCast2Talkable(interactableLeft);
                if (character != null) {
                    character.hasTalked = false;
                }

                if (lastTriggeredInteractable == interactableLeft) {
                    lastTriggeredInteractable = null;
                }
            }
        }
    }

    protected void OnTriggerStay(Collider other) {
        if (other.tag == "NPC") {
            Interactable stayedInteractable = other.GetComponent<Interactable>();

            if (lastTriggeredInteractable == null) {
                lastTriggeredInteractable = stayedInteractable;
                lastTriggeredInteractable.canInteract = true;

                Talkable character = TryCast2Talkable(lastTriggeredInteractable);
                if (character != null) {
                    character.hasTalked = false;
                }

            }
            else if (lastTriggeredInteractable != stayedInteractable) {
                stayedInteractable.canInteract = false;
            }
        }
    }

    private Talkable TryCast2Talkable(Interactable interactable) {
        try {
            Talkable character = (Talkable)interactable;
            return character;
        }
        catch (Exception e) {
            Debug.Log("Not talkable");
        }
        return null;
    }


}
