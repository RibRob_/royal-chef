﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class ControllerInput : MonoBehaviour {

    public bool playerIndexSet = false;
    public PlayerIndex playerIndex;
    public GamePadState gamePadState;
    public GamePadState previousGamePadState;
    public float deadZone = 0.08f;

    public Transform playerTransform;
    public float speed = 3f;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        UpdateGamePad();
        Walk();
    }

    private void FixedUpdate() {
        Rumble();
    }

    private void UpdateGamePad() {
        //Check how many controllers are present
        if (!playerIndexSet || !previousGamePadState.IsConnected) {
            for (int i = 0; i < 4; ++i) {
                PlayerIndex testPlayerIndex = (PlayerIndex)i;
                GamePadState testState = GamePad.GetState(testPlayerIndex);
                if (testState.IsConnected) {
                    Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
                    playerIndex = testPlayerIndex;
                    playerIndexSet = true;
                }
            }
        }

        previousGamePadState = gamePadState;
        gamePadState = GamePad.GetState(PlayerIndex.One);
    }
    
    private void Walk() {
        if (Mathf.Abs(gamePadState.ThumbSticks.Left.X) >= deadZone || Mathf.Abs(gamePadState.ThumbSticks.Left.Y) >= deadZone) {
            Vector3 movement = new Vector3(gamePadState.ThumbSticks.Left.X, 0, gamePadState.ThumbSticks.Left.Y);
            movement = movement * Time.deltaTime * speed;
            playerTransform.Translate(movement);
        }
    }

    private void Rumble() {
        //A button was just pressed
        if (previousGamePadState.Buttons.A == ButtonState.Released && gamePadState.Buttons.A == ButtonState.Pressed) {
        }
        //A button is being held
        else if (previousGamePadState.Buttons.A == ButtonState.Pressed && gamePadState.Buttons.A == ButtonState.Pressed) {
            GamePad.SetVibration(PlayerIndex.One, 0.5f, 0.5f);
        }
        //A button is let go
        else if (previousGamePadState.Buttons.A == ButtonState.Released && gamePadState.Buttons.A == ButtonState.Released) {
            GamePad.SetVibration(PlayerIndex.One, 0f, 0f);
        }
    }
}